import request from '@/utils/request'

// 获取全部字典
export function getDictAll() {
  return request({
    url: '/dict/getAll',
    method: 'get'
  })
}

// 根据id获取字典
export function getDict() {
  return request({
    url: '/dict/get',
    method: 'get'
  })
}
