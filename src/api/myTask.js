import request from '@/utils/request'
import settings from '@/settings'

const act = settings['Ark-Activiti']

/* 待办*/
export function getupcoming(data) {
  return request({
    url: act + '/myTask/getupcoming',
    method: 'post',
    // 'content-type': 'application/json',
    data: data
  })
}
/* 已办*/
export function getDone(data) {
  return request({
    url: act + '/myTask/getDone',
    method: 'post',
    data: data
  })
}
/* 流程*/
export function getLaunch(data) {
  return request({
    url: act + '/myTask/getLaunch',
    method: 'post',
    data: data
  })
}
/* 提交任务 */
export function complete(data) {
  return request({
    url: act + '/proManagement/complete',
    method: 'post',
    data: data
  })
}
/* 查询最新版本流程定义 */
export function getProcessLastVersion() {
  return request({
    url: act + '/proManagement/getProcessLastVersion',
    method: 'get'
  })
}
/* 根据流程key查看流程图 */
export function processImage(data) {
  return request({
    url: act + '/proManagement/processImage/' + data,
    method: 'post',
    responseType: 'blob'
  })
}
/* 根据实例id查看流程图状态 */
export function processImageStatus(data) {
  return request({
    url: act + '/proManagement/processImageStatus/' + data,
    method: 'post'
  })
}
/* 根据实例id查看流程图状态 */
export function deleteByKey(data) {
  return request({
    url: act + '/proManagement/deleteByKey/' + data,
    method: 'delete'
  })
}
export function deploy(p1, p2) {
  return request({
    url: act + '/proManagement/deploy/' + p1,
    method: 'post',
    data: p2
  })
}
export function getComments(data) {
  return request({
    url: act + '/proManagement/getComments/' + data,
    method: 'get'
  })
}
