import request from '@/utils/request'

export function sysLogin(data) {
  const { username, password } = data
  return request({
    url: '/sys/login?username=' + username + '&password=' + password,
    method: 'post'
  })
}
export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/sys/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/sys/vue-admin-template/user/logout',
    method: 'post'
  })
}

export function getUserAll() {
  return request({
    url: '/sys/user/getAll',
    method: 'get'
  })
}
export function deleteUser(params) {
  return request({
    url: '/sys/user/' + params,
    method: 'delete'
  })
}
export function editUser(params) {
  return request({
    url: '/sys/user/',
    method: 'post',
    data: params
  })
}
export function addUser(params) {
  return request({
    url: '/sys/user/',
    method: 'put',
    data: params
  })
}
