import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    meta: {
      breadcrumb: false
    },
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },
  {
    path: '/bpmn',
    name: 'bpmn',
    component: Layout,
    alwaysShow: true,
    redirect: 'noRedirect',
    meta: {
      title: '流程管理'
    },
    children: [
      {
        path: 'news',
        name: 'news',
        component: () => import('@/views/bpmn/news/index'),
        meta: { title: '我的消息' }
      },
      {
        path: 'definition',
        name: 'definition',
        component: () => import('@/views/bpmn/definition/index'),
        meta: { title: '流程定义' }
      },
      {
        path: 'state',
        name: 'state',
        component: () => import('@/views/bpmn/state/index'),
        meta: { title: '节点状态' }
      },
      {
        path: 'news',
        name: 'news',
        component: () => import('@/views/bpmn/news/index'),
        meta: { title: '类型管理' }
      },
      {
        path: 'bpmn',
        name: 'bpmn设计器',
        component: () => import('@/views/bpmn/designer/index'),
        meta: { title: 'bpmn设计器' }
      }
    ]
  },
  {
    path: '/orgm',
    component: Layout,
    name: 'orgm',
    redirect: 'noRedirect',
    alwaysShow: true,
    meta: {
      title: '组织管理',
      icon: ''
    },
    children: [
      {
        path: 'user',
        name: 'user',
        component: () => import('@/views/orgm/user/index'),
        meta: { title: '用户管理' }
      }
    ]
  },
  {
    path: '/business',
    component: Layout,
    name: 'business',
    redirect: 'noRedirect',
    alwaysShow: true,
    meta: {
      title: '业务配置',
      icon: ''
    },
    children: [
      {
        path: 'type',
        name: 'type',
        component: () => import('@/views/business/type/index'),
        meta: { title: '类型管理' }
      }
    ]
  },
  {
    path: '/systemconifg',
    component: Layout,
    name: 'systemconifg',
    redirect: 'noRedirect',
    meta: {
      title: '系统配置',
      icon: ''
    },
    children: [
      {
        path: 'dict',
        name: 'dict',
        component: () => import('@/views/systemconifg/dict/index'),
        meta: { title: '字典配置' }
      },
      {
        path: 'sms',
        name: 'sms',
        component: () => import('@/views/systemconifg/sms/index'),
        meta: { title: '短信配置' }
      },
      {
        path: 'mail',
        name: 'mail',
        component: () => import('@/views/systemconifg/mail/index'),
        meta: { title: '邮件配置' }
      }
    ]
  },

  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
        meta: { title: 'External Link', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
