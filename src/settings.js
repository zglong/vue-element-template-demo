module.exports = {

  'Ark-Activiti': '/act',
  'Ark-Sys': '/sys',
  title: 'Vue Admin Template',

  /**
   * @type {boolean} true | false
   * @description Whether fix the headerlocalhost:9000/act-zglong/
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: false
}
