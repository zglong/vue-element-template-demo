export default [
  {
    url: '/myTask/getupcoming',
    type: 'get',
    response: _ => {
      return {
        'success': true,
        'message': 'success',
        'code': 20000,
        'timestamp': 1599391925699,
        'result': [
          {
            'id': '311420186123046912',
            'task': '112620',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112611',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '填写申请单',
            'nodeStatus': '填写申请单',
            'type': 'task'
          },
          {
            'id': '311426108866170880',
            'task': '112650',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112638',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311426261870186496',
            'task': '112663',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112651',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311430861784354816',
            'task': '112676',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112664',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311430925864931328',
            'task': '112689',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112677',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311432020808962048',
            'task': '112714',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112702',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311432070570184704',
            'task': '112727',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112715',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311432289567379456',
            'task': '112744',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112732',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311446561932447744',
            'task': '112813',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '112801',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311447179094921216',
            'task': '115017',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '115005',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311448329181466624',
            'task': '115032',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '115020',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311448603723829248',
            'task': '115049',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '115037',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '311448881512583168',
            'task': '117513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '117501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312171559739461632',
            'task': '122513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '122501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312173327206584320',
            'task': '127513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '127501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312174125978226688',
            'task': '130013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '130001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312179188943556608',
            'task': '142513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '142501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312179619916681216',
            'task': '145013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '145001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312180142078169088',
            'task': '147513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312180205949030400',
            'task': '147526',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147514',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312180453345857536',
            'task': '147539',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147527',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312180495376977920',
            'task': '147552',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147540',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312181435941261312',
            'task': '147565',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147553',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312181603281408000',
            'task': '147578',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147566',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312181867249930240',
            'task': '147591',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '147579',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312182652008402944',
            'task': '150013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '150001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312182919315591168',
            'task': '152513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '152501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312186402114637824',
            'task': '157513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '157501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312186887114592256',
            'task': '160013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '160001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312187260994850816',
            'task': '162513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '162501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312188113428418560',
            'task': '162526',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '162514',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312188524436656128',
            'task': '165013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '165001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312188805954146304',
            'task': '165026',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '165014',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312188906906849280',
            'task': '165039',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '165027',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312195547794509824',
            'task': '167513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '167501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312346888030392320',
            'task': '170013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '170001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312365324601135104',
            'task': '172513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '172501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312365841888841728',
            'task': '172526',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '172514',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312374281050787840',
            'task': '175013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '175001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312374405508370432',
            'task': '175026',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '175014',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312374785646530560',
            'task': '177513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '177501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312374919885230080',
            'task': '177526',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '177514',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312375906477805568',
            'task': '177539',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '177527',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312379242786590720',
            'task': '182513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '182501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312379789765775360',
            'task': '185013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '185001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312381064226017280',
            'task': '185026',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '185014',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312394067830902784',
            'task': '202513',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '202501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312394759001870336',
            'task': '205013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '205001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312397467486261248',
            'task': '205030',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '205018',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312397493184761856',
            'task': '205043',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '205031',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312398694840274944',
            'task': '205058',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '205046',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312398928379121664',
            'task': '207510',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '207501',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '填写申请单',
            'nodeStatus': '填写申请单',
            'type': 'task'
          },
          {
            'id': '312399153734881280',
            'task': '207521',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '207512',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '填写申请单',
            'nodeStatus': '填写申请单',
            'type': 'task'
          },
          {
            'id': '312399264267374592',
            'task': '207535',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '207523',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312400085298188288',
            'task': '207552',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '207540',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312400629085507584',
            'task': '210017',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '210005',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312400816898052096',
            'task': '210030',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '210018',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          },
          {
            'id': '312402685833777152',
            'task': '215013',
            'userId': 'helong',
            'proCreateUserId': 'helong',
            'proId': '215001',
            'processKey': 'Student',
            'entityId': 'zglong',
            'entityClass': 'cn.zglong.ark.activiti.entity.Student',
            'entityName': '306746305240109056',
            'nodeTitle': '班主任批准',
            'nodeStatus': '班主任批准',
            'type': 'task'
          }
        ]
      }
    }
  },
  {
    url: '/proManagement/complete',
    method: 'post',
    response: _ => {
      return {
        'success': true,
        'message': 'success',
        'code': 20000,
        'timestamp': 1599391925699,
        'result': [
          { 'aa': 123 }
        ]
      }
    }
  }
]
