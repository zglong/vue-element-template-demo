
const tokens = {
  admin: {
    token: 'admin-token'
  },
  zglong: {
    token: 'admin-token'
  },
  editor: {
    token: 'editor-token'
  }
}

const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

export default [
  {
    url: "/sys/user/",
    type: "post",
    response: _ => {
      return {
        "success": true,
        "message": "保存成功",
        "code": 20000,
        "timestamp": 1592058200579,
        "result": null
      }
    }
  },
  {
    url: "/sys/user/",
    type: "delete",
    response: _ => {
      return {
        "success": true,
        "message": "删除成功",
        "code": 20000,
        "timestamp": 1592058200579,
        "result": null
      }
    }
  },
  {
    url: '/sys/user/getAll',
    type: 'get',
    response: _ => {
      return {
        "success": true,
        "message": "success",
        "code": 20000,
        "timestamp": 1592054569216,
        "result": [
          {
            "id": "280630763076980736",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-03T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "zglong",
            "fullname": null,
            "password": "123456",
            "nickname": null,
            "orgId": null,
            "phone": "18215399770",
            "email": "1@qq.com",
            "sex": "女",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280633694169862144",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-04T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "xjj",
            "fullname": null,
            "password": "123456",
            "nickname": null,
            "orgId": null,
            "phone": "18215399771",
            "email": "2@qq.com",
            "sex": "女",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280637667148828672",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-05T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "123",
            "fullname": null,
            "password": "123456",
            "nickname": null,
            "orgId": null,
            "phone": "18215399772",
            "email": "3@qq.com",
            "sex": "男",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280639154352558080",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-06T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "123",
            "fullname": null,
            "password": "123456",
            "nickname": null,
            "orgId": null,
            "phone": "18215399773",
            "email": "4@qq.com",
            "sex": "女",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280642326588559360",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-07T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": null,
            "fullname": null,
            "password": "123456",
            "nickname": null,
            "orgId": null,
            "phone": "18215399774",
            "email": "5@qq.com",
            "sex": "男",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280642684022951936",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-08T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": null,
            "fullname": null,
            "password": "123456",
            "nickname": null,
            "orgId": null,
            "phone": "18215399775",
            "email": "6@qq.com",
            "sex": "男",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280952889902370816",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-09T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "a",
            "fullname": null,
            "password": "572153b0ba84633c15dfb627180a869a",
            "nickname": null,
            "orgId": null,
            "phone": "18215399776",
            "email": "7@qq.com",
            "sex": "女",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280972960649449472",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-10T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "d",
            "fullname": null,
            "password": "572153b0ba84633c15dfb627180a869a",
            "nickname": null,
            "orgId": null,
            "phone": "18215399777",
            "email": "8@qq.com",
            "sex": "男",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "280988721975660544",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-11T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "c",
            "fullname": null,
            "password": "$2a$10$PmgVTfzcK5PBR4eD4lA4H.ZAGY6LGlSSAvFyKoqcq7Uj6qpDGUi9O",
            "nickname": null,
            "orgId": null,
            "phone": "18215399778",
            "email": "9@qq.com",
            "sex": "男",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          },
          {
            "id": "282438855922028544",
            "revision": null,
            "createdBy": null,
            "createdTime": "2020-06-12T21:58:00",
            "updatedBy": null,
            "updatedTime": null,
            "deleted": null,
            "username": "adminadmin",
            "fullname": null,
            "password": "$2a$10$Ny8184LguzlUlHygdV/j.ee0VCFkb/ZjBeNoLOb.bqtLiW7hKWuMa",
            "nickname": null,
            "orgId": null,
            "phone": "18215399779",
            "email": "10@qq.com",
            "sex": "男",
            "status": "ENABLE",
            "avatar": null,
            "groups": null,
            "organizes": null,
            "positions": null
          }
        ]
      }

    }

  },
  // user login
  {
    url: '/sys/login',
    type: 'post',
    response: config => {
      const { username } = config.body
      const token = tokens[username]
console.log(token)
      // mock error
      if (!token) {
        return {
          code: 60204,
          message: 'Account and password are incorrect.'
        }
      }

      return {
        code: 20000,
        message: "登录成功",
        result: {token: 'admin-token'},
        success: true,
        timestamp: 1592145834924
      }
    }
  },

  // get user info
  {
    url: '/sys/user/info\.*',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: 'Login failed, unable to get user details.'
        }
      }

      return {
        code: 20000,
        data: info
      }
    }
  },

  // user logout
  {
    url: '/sys/vue-admin-template/user/logout',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]
